Protocol definition
===================

Overview
--------

.. todo::

    Give protocol overview.

.. todo::

    Talk about flatbuffers_.

Structures and types
--------------------

.. literalinclude:: ../batprotocol.fbs
   :language: text

.. _flatbuffers: https://google.github.io/flatbuffers/
