.. batprotocol documentation master file, created by
   sphinx-quickstart on Mon Jul 19 17:16:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

batprotocol
===========

.. toctree::
   :maxdepth: 1
   :caption: User Manual:

   Installation <installation.rst>
   Getting started <getting-started.rst>
   Protocol definition <protocol-definition.rst>
   High-level API reference <api.rst>
